import {
  Injectable,
  NestInterceptor,
  ExecutionContext, CallHandler
} from '@nestjs/common'
import { Observable } from 'rxjs'
import { ContextModel } from './context.model'

@Injectable()
export class ContextInterceptor<T>
  implements NestInterceptor<T, T> {

  public intercept (
    executionContext: ExecutionContext,
    next: CallHandler
  ): Observable<T> {
    const httpContext = executionContext.switchToHttp()

    const request = httpContext.getRequest()
    const response = httpContext.getResponse()

    if (request.context) {
      response.context = request.context
    } else {
      const context = new ContextModel()

      request.context = context
      response.context = context
    }

    return next.handle()
  }
}
