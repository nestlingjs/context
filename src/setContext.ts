import { ContextModel } from './context.model'

export function setContext <RequestType = any> (
  key: string | string[],
  value: any,
  request: RequestType
): RequestType {
  if (!(request as any).context) {
    (request as any).context = new ContextModel()
  }

  (request as any).context.set(key, value)

  return request
}
