import * as _get from 'lodash.get'
import * as _has from 'lodash.has'
import * as _set from 'lodash.set'

export class ContextModel {
  private _context: any = {}

  public get<T = any> (path): T {
    return _get<T>(this._context, path)
  }

  public has (path): boolean {
    return _has(this._context, path)
  }

  public set (path, value: any): void {
    _set(this._context, path, value)
  }
}
