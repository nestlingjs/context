import { ContextModel } from './context.model'

export function getContext <T = any> (
  path: string | string[],
  request: any
): T | null {
  if (!request.context) {
    return null
  }

  const context: ContextModel = request.context

  return context.get<T>(path)
}
