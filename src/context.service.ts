import { Injectable } from '@nestjs/common'
import { ContextModel } from './context.model'

@Injectable()
export class ContextService extends ContextModel {}
