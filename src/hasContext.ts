export function hasContext (
  path: string | string[],
  request: any
): boolean {
  if (!request.context) {
    return false
  }

  return request.context.has(path)
}
