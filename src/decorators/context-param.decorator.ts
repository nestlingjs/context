import { createParamDecorator, PipeTransform } from '@nestjs/common'

export const Context = createParamDecorator(
  (path, request): PipeTransform => {
    if (request.context) {
      if (path) {
        if (request.context.has(path)) {
          return request.context.get(path)
        }
      } else {
        return request.context
      }
    }

    throw Error('Request context is invalid.')
  }
)
